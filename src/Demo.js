import React from "react";
import { Column, Table } from "react-virtualized";
import Draggable from "react-draggable";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";
import "./styles.css";

// const width = 500;

export default class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.table = React.createRef();
  }

  state = {
    page: 1,
    primaryRow: [
      {
        text: "Модальность",
        index: 7,
        isDesc: true,
        dataKey: "modality",
        width: 1 / 12
      },
      {
        text: "Имя пациента",
        index: 8,
        isDesc: true,
        dataKey: "name",
        width: 1 / 12
      },
      {
        text: "Номер пациента",
        index: 10,
        isDesc: true,
        dataKey: "patientNumber",
        width: 1 / 12
      },
      {
        text: "Пол",
        index: 11,
        isDesc: true,
        dataKey: "gender",
        width: 1 / 12
      },
      {
        text: "Возраст",
        index: 12,
        isDesc: true,
        dataKey: "age",
        width: 1 / 12
      },
      {
        text: "Дата рожд.",
        index: 13,
        isDesc: true,
        dataKey: "birthDate",
        width: 1 / 12
      },
      {
        text: "Дата/Время",
        index: 14,
        isDesc: true,
        dataKey: "date",
        width: 1 / 12
      },
      {
        text: "Направл. врач",
        index: null,
        isDesc: true,
        dataKey: "referral",
        width: 0.5 / 12
      },
      {
        text: "Описание",
        index: null,
        isDesc: true,
        dataKey: "description",
        width: 1 / 12
      },
      {
        text: "Рег. номер",
        index: 17,
        isDesc: true,
        dataKey: "serialNumber",
        width: 1 / 12
      },
      {
        text: "Дата/Время загрузки",
        index: 18,
        isDesc: true,
        dataKey: "timeDownload",
        width: 1 / 12
      },
      {
        text: "Приоритет",
        index: null,
        isDesc: true,
        dataKey: "priority",
        width: 1 / 12
      },
      {
        text: "AE Title",
        index: 20,
        isDesc: true,
        dataKey: "AEtitle",
        width: 1 / 12
      },
      {
        text: "Учреждение",
        index: 21,
        isDesc: true,
        dataKey: "institution",
        width: 1 / 12
      }
    ],
    widths: {
      name: 1 / 12,
      gender: 0.4 / 12,
      modality: 1 / 12,
      serialNumber: 1 / 12,
      age: 0.5 / 12,
      birthDate: 1 / 12,
      date: 1 / 12,
      timeDownload: 1 / 12,
      patientNumber: 1 / 12,
      AEtitle: 0.5 / 12,
      institution: 1 / 12,
      description: 1 / 12,
      referral: 1 / 12,
      priority: 1 / 12
    },
    width: 500,
    list: []
    // widths: {
    //   name: 0.33,
    //   location: 0.33,
    //   description: 0.33
    // }
  };

  onClick = (filter, sort) => {
    axios({
      method: "get",
      headers: {
        Accept: "application/json"
      },
      url:
        "http://lkmt.kometa-pacs.info/webpbdata?type=filter&param=Select%2CAllFilter%2C1%2CAccessionNumber%2Cdesc&ownerId=7&ownerType=1&isWorklist=1&timezone=4&itemsPerPage=23&fpc=1&_search=false&nd=1577707872310&rows=1000&page=" +
        this.state.page +
        "&sidx=AccessionNumber&sord=desc"
    }).then(res => {
      const list = res.data.rows;
      const patientData = list.map(patient => {
        const patientData = patient.cell;
        const name = patientData[8];
        const gender = patientData[11];
        const modality = patientData[7];
        const serialNumber = patientData[10];
        const age = patientData[12];
        const birthDate = patientData[13];
        const date = patientData[14];
        const timeDownload = patientData[18];
        const patientNumber = patientData[17];
        const AEtitle = patientData[20];
        const institution = patientData[21];
        return {
          name,
          gender,
          modality,
          serialNumber,
          age,
          birthDate,
          date,
          timeDownload,
          patientNumber,
          AEtitle,
          institution
        };
      });

      this.setState(prevState => ({
        ...prevState,
        list: patientData
      }));
    });
  };

  componentDidMount() {
    const width = document.querySelector(".ReactVirtualized__Table")
      .offsetWidth;
    this.setState(prevState => ({ ...prevState, width }));
    axios({
      method: "get",
      headers: {
        Accept: "application/json"
      },
      url:
        "http://lkmt.kometa-pacs.info/webpbdata?type=filter&param=Select%2CAllFilter%2C1%2CAccessionNumber%2Cdesc&ownerId=7&ownerType=1&isWorklist=1&timezone=4&itemsPerPage=23&fpc=1&_search=false&nd=1577707872310&rows=1000&page=" +
        this.state.page +
        "&sidx=AccessionNumber&sord=desc"
    }).then(res => {
      const list = res.data.rows;
      const patientData = list.map(patient => {
        const patientData = patient.cell;
        const name = patientData[8];
        const gender = patientData[11];
        const modality = patientData[7];
        const serialNumber = patientData[10];
        const age = patientData[12];
        const birthDate = patientData[13];
        const date = patientData[14];
        const timeDownload = patientData[18];
        const patientNumber = patientData[17];
        const AEtitle = patientData[20];
        const institution = patientData[21];
        return {
          name,
          gender,
          modality,
          serialNumber,
          age,
          birthDate,
          date,
          timeDownload,
          patientNumber,
          AEtitle,
          institution
        };
      });

      this.setState(prevState => ({
        ...prevState,
        list: patientData
      }));
    });

    if (this.props.resizable) {
      this.enableResize();
    }
  }

  render() {
    const { list } = this.state;
    const { widths } = this.state;
    console.log(this.state.primaryRow[0].width);
    return (
      <React.Fragment>
        <Table
          className="table-dark"
          width={this.state.width}
          height={300}
          headerHeight={20}
          rowHeight={30}
          rowCount={list.length}
          rowGetter={({ index }) => list[index]}
        >
          {this.state.primaryRow.map((row, index) => (
            <Column
              headerRenderer={() =>
                this.headerRenderer(
                  row.dataKey,
                  !this.state.primaryRow[index + 1]
                    ? null
                    : this.state.primaryRow[index + 1].dataKey,
                  row.text,
                  () => this.onClick(row.dataKey, row.text),
                  index
                )
              }
              dataKey={row.dataKey}
              label={row.text}
              // width={row.width}
              width={widths[row.dataKey] * this.state.width}
            />
          ))}
        </Table>
        <div>
          <button
            onClick={() => {
              axios({
                method: "get",
                headers: {
                  Accept: "application/json"
                },
                url:
                  "http://lkmt.kometa-pacs.info/webpbdata?type=filter&param=Select%2CAllFilter%2C1%2CAccessionNumber%2Cdesc&ownerId=7&ownerType=1&isWorklist=1&timezone=4&itemsPerPage=23&fpc=1&_search=false&nd=1577707872310&rows=1000&page=" +
                  (this.state.page - 1) +
                  "&sidx=AccessionNumber&sord=desc"
              }).then(res => {
                const list = res.data.rows;
                const patientData = list.map(patient => {
                  const patientData = patient.cell;
                  const name = patientData[8];
                  const gender = patientData[11];
                  const modality = patientData[7];
                  const serialNumber = patientData[10];
                  const age = patientData[12];
                  const birthDate = patientData[13];
                  const date = patientData[14];
                  const timeDownload = patientData[18];
                  const patientNumber = patientData[17];
                  const AEtitle = patientData[20];
                  const institution = patientData[21];
                  return {
                    name,
                    gender,
                    modality,
                    serialNumber,
                    age,
                    birthDate,
                    date,
                    timeDownload,
                    patientNumber,
                    AEtitle,
                    institution
                  };
                });

                this.setState(prevState => ({
                  ...prevState,
                  list: patientData,
                  page: prevState.page - 1
                }));
              });
            }}
          >
            Previous
          </button>
          {this.state.page}

          <button
            onClick={() => {
              axios({
                method: "get",
                headers: {
                  Accept: "application/json"
                },
                url:
                  "http://lkmt.kometa-pacs.info/webpbdata?type=filter&param=Select%2CAllFilter%2C1%2CAccessionNumber%2Cdesc&ownerId=7&ownerType=1&isWorklist=1&timezone=4&itemsPerPage=23&fpc=1&_search=false&nd=1577707872310&rows=1000&page=" +
                  (this.state.page + 1) +
                  "&sidx=AccessionNumber&sord=desc"
              }).then(res => {
                const list = res.data.rows;
                const patientData = list.map(patient => {
                  const patientData = patient.cell;
                  const name = patientData[8];
                  const gender = patientData[11];
                  const modality = patientData[7];
                  const serialNumber = patientData[10];
                  const age = patientData[12];
                  const birthDate = patientData[13];
                  const date = patientData[14];
                  const timeDownload = patientData[18];
                  const patientNumber = patientData[17];
                  const AEtitle = patientData[20];
                  const institution = patientData[21];
                  return {
                    name,
                    gender,
                    modality,
                    serialNumber,
                    age,
                    birthDate,
                    date,
                    timeDownload,
                    patientNumber,
                    AEtitle,
                    institution
                  };
                });
                this.setState(prevState => ({
                  ...prevState,
                  list: patientData,
                  page: prevState.page + 1
                }));
              });
            }}
          >
            Next
          </button>
        </div>
      </React.Fragment>
    );
  }

  // {
  //   columnData,
  //   dataKey,
  //   disableSort,
  //   label,
  //   sortBy,
  //   sortDirection
  // }

  headerRenderer = (dataKey, nextDataKey, label, onClickFun, index) => {
    console.log(label);
    return (
      <React.Fragment key={dataKey}>
        <div
          className="ReactVirtualized__Table__headerTruncatedText"
          style={{ width: "100%" }}
          onClick={onClickFun}
        >
          {label}
        </div>
        <Draggable
          axis="x"
          defaultClassName="DragHandle"
          defaultClassNameDragging="DragHandleActive"
          onDrag={(event, { deltaX }) =>
            this.resizeRow({
              dataKey,
              deltaX,
              nextDataKey
            })
          }
          position={{ x: 0 }}
          zIndex={999}
        >
          <span className="DragHandleIcon">⋮</span>
        </Draggable>
      </React.Fragment>
    );
  };

  // resizeRow = ({ dataKey, deltaX, index }) => {
  //   const percentDelta = deltaX / this.state.width;
  //   const primaryRow = [...this.state.primaryRow];
  //   primaryRow[index].width += percentDelta;
  //   primaryRow[index + 1].width -= percentDelta;

  //   this.setState(prevState => ({ ...prevState, primaryRow }));
  //   // const column = this.state.primaryRow[index + 1];
  //   // const nextDataKey = column.dataKey;
  //   // this.setState(prevState => ({
  //   //   ...prevState,
  //   //   primaryRow
  //   // }));
  //   // this.setState(prevState => {
  //   //   const prevWidths = prevState.widths;

  //   //   return {
  //   //     widths: {
  //   //       ...prevWidths,
  //   //       [dataKey]: prevWidths[dataKey] + percentDelta,
  //   //       [nextDataKey]: prevWidths[nextDataKey] - percentDelta
  //   //     }
  //   //   };
  //   // });
  // };
  resizeRow = ({ dataKey, deltaX, nextDataKey }) =>
    this.setState(prevState => {
      const prevWidths = prevState.widths;
      const percentDelta = deltaX / this.state.width;
      return {
        widths: {
          ...prevWidths,
          [dataKey]: prevWidths[dataKey] + percentDelta,
          [nextDataKey]: prevWidths[nextDataKey] - percentDelta
        }
      };
    });
}
